"use strict";

// Getting our hostname
var hostname = window.location.hostname;
if (!hostname) {
    hostname = "localhost";
}

// Defining the connection to our WebSocket backend
var conn = new WebSocket('ws://' + hostname + ":8080/socket");

// What to do when the data channel is initially opened
conn.onopen = async function() {
    console.log("Connected to the signalling server");
    await initialize();
    console.log("Initialized");
};

// What to do when we receive a message from the WebSocket backend
conn.onmessage = function(msg) {
    console.log("Got Message");
    var content = JSON.parse(msg.data);
    var data = content.data;
    switch(content.event) {
        case "offer":
            handleOffer(data);
            break;
        case "answer":
            handleAnswer(data);
            break;
        case "candidate":
            handleCandidate(data);
            break;
        default:
            break;
    }
};

// The send utility function
function send(message) {
    conn.send(JSON.stringify(message));
}

// Defining the variables
var peerConnection; // The RTCPeerConnection Object
var dataChannel;    // A data channel to the remote peer
var stream;         // The video stream we're going to capture

// Video stream constraints
const constraints = {
    audio: true,
    video: true,
};

// Initializes our connection
async function initialize() {
    console.log("initializing");

    // Configuring the Peer Connection and STUN server settings
    //var configuration = null;
    var configuration = {'iceServers': [{'urls': 'stun:stun.l.google.com:19302'}]};
    peerConnection = new RTCPeerConnection(configuration);

    // Setup ice
    peerConnection.onicecandidate = function(event) {
        if (event.candidate) {
            send({
                event: "candidate",
                data: event.candidate
            });
        }
    };

    // Captures the video
    stream = await navigator.mediaDevices.getUserMedia(constraints);

    // HTML Object linking
    const localVideo = document.querySelector('video#local_video');
    const receivedVideo = document.querySelector('video#received_video')
    localVideo.srcObject = stream;

    // Creating the data channel
    dataChannel = peerConnection.createDataChannel("dataChannel", {
        reliable: true
    });

    dataChannel.onerror = function(error) {
        console.log("DataChannel error", error);
    };

    dataChannel.onmessage = function(event) {
        console.log("Data Channel Got message");
    };

    dataChannel.onclose = function() {
        console.log("Closed");
    };

    dataChannel.ondatachannel = function(event) {
        dataChannel = event.channel;
    };

    // Attaches our video stream to the peer connection object
    peerConnection.addStream(stream);

    // What to do when we receive a stream
    peerConnection.onaddstream = function(event) {
        receivedVideo.srcObject = event.stream;
    };
}

// Create and send an offer to the other party
function createOffer() {
    peerConnection.createOffer(function(offer) {
        send({
            event: "offer",
            data: offer
        });
        peerConnection.setLocalDescription(offer);
    }, function(error) {
        alert("Failed to create offer");
    });
}

// Handler for when we receive an offer
function handleOffer(offer) {
    peerConnection.setRemoteDescription(new RTCSessionDescription(offer));

    peerConnection.createAnswer(function(answer) {
        peerConnection.setLocalDescription(answer);
        send({
            event: "answer",
            data: answer
        });
    }, function(error) {
        alert("Failed to create answer");
    });
};

// Adds a new ICE candidate upon receiving one
function handleCandidate(candidate) {
    peerConnection.addIceCandidate(new RTCIceCandidate(candidate));
};

// Handles an answer
function handleAnswer(answer) {
    peerConnection.setRemoteDescription(new RTCSessionDescription(answer));
    console.log("Connection established");
};
