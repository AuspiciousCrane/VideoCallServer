package com.example.VideoCallServer;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class VideoCallServerApplication {

	public static void main(String[] args) {
		SpringApplication.run(VideoCallServerApplication.class, args);
	}

}
